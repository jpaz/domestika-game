﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 2f;
    public Vector2 direction;
    public float livingTime = 3f;
    public Color initialColor = Color.white;
    public Color finalColor;
    private SpriteRenderer spriteRenderer;
    private float startingTime;

    private void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>(); 
    }

    // Start is called before the first frame update
    void Start()
    {
        // Save initial time
        startingTime = Time.time;
        // Destroy the bullet after some time
        Destroy(this.gameObject, this.livingTime);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 movement = direction.normalized * speed * Time.deltaTime;
        // transform.position = new Vector2(transform.position.x + movement.x, transform.position.y + movement.y);
        transform.Translate(movement);

        // Charge bullet's color over time
        float timeSinceStarted = Time.time - startingTime;
        float percentageCompleted = timeSinceStarted / livingTime;

        spriteRenderer.color = Color.Lerp(initialColor, finalColor, percentageCompleted);
    }
}
