﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject bulletPrefab;
    public GameObject shooter;
    private Transform firePoint;

    private void Awake()
    {
        this.firePoint = transform.Find("FirePoint");
    }

    // Start is called before the first frame update
    void Start()
    {
        // Instantiate(bulletPrefab, firePoint.position, Quaternion.identity);
        // Invoke("Shoot", 1f);
        // Invoke("Shoot", 2f);
        // Invoke("Shoot", 3f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Shoot()
    {
        if (bulletPrefab != null && firePoint != null)
        {
            GameObject myBullet = Instantiate(bulletPrefab, firePoint.position, Quaternion.identity) as GameObject;
            Bullet bulletComponent = myBullet.GetComponent<Bullet>();

            if (shooter.transform.localScale.x < 0f)
            {
                bulletComponent.direction = Vector2.left; // new Vector2(-1f, 0f);
            }
            else
            {
                bulletComponent.direction = Vector2.right; // new Vector2(1f, 0f);
            }
        }
    }
}
