﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public float speed = 1f;
    public float minX;
    public float maxX;
    public float waitingTime = 2f;
    private Weapon weapon;
    private GameObject target;
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        weapon = GetComponentInChildren<Weapon>();
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdateTarget();
        StartCoroutine("PatrolToTarget");
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void Shoot()
    {
        if (weapon != null)
        {
            weapon.Shoot();
        }
    }

    private void UpdateTarget()
    {
        // If first tim, create target in the left
        if (target == null)
        {
            target = new GameObject("Target");
            target.transform.position = new Vector2(minX, transform.position.y);
            transform.localScale = new Vector3(-1, 1, 1);
            return;
        }
        // If we are in the left, change target to the right
        if (target.transform.position.x == minX)
        {
            target.transform.position = new Vector2(maxX, transform.position.y);
            transform.localScale = new Vector3(1, 1, 1);
        }
        // If we are int the right, change target to the left
        else if (target.transform.position.x == maxX)
        {
            target.transform.position = new Vector2(minX, transform.position.y);
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    IEnumerator PatrolToTarget()
    {
        // Caroutine to move the enemy
        while (Vector2.Distance(transform.position, target.transform.position) > 0.05f)
        {
            // Update animator
            animator.SetBool("Idle", false);

            // Let's move to the target
            Vector2 direction = target.transform.position - transform.position;
            transform.Translate(direction.normalized * speed * Time.deltaTime);
            // IMPORTANT
            yield return null;
        }
        // At this point, i've reached the target, let's set our position to the target's one
        Debug.Log("Target reached");
        transform.position = new Vector2(target.transform.position.x, transform.position.y);
        UpdateTarget();

        // Update animator
        animator.SetBool("Idle", true);
        animator.SetTrigger("Shoot");

        // And let's wait for a moment
        Debug.Log("Waiting for " + waitingTime + " seconds");
        yield return new WaitForSeconds(waitingTime); // IMPORTANT
        // Once waited, let's restore the patrol behaviour
        Debug.Log("Waited enough, let's update the target and move again");
        StartCoroutine("PatrolToTarget");
    }
}
