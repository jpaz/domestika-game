﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Stats
    public float speed = 1.85f;
    public float jumpForce = 2.5f;
    public float longIdleTime = 5f;
    // Jump
    public Transform groundCheck;
    public LayerMask groundLayer;
    public float groundCheckRadius;
    // References
    private Rigidbody2D rigibody;
    private Animator animator;
    // Movement
    private Vector2 movement;
    private bool facingRight = true;
    private bool isGrounded;
    // Attack
    private bool isAttacking;
    // Long idle
    private float longIdleTimer;

    private void Awake()
    {
        rigibody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void Start()
    {

    }

    private void Update()
    {
        UpdateMovement();
        UpdateAttack();
    }

    private void FixedUpdate()
    {
        float HorizontalVelocity = movement.normalized.x * speed;
        rigibody.velocity = new Vector2(HorizontalVelocity, rigibody.velocity.y);
    }

    private void LateUpdate()
    {
        animator.SetBool("Idle", movement == Vector2.zero);
        animator.SetBool("IsGrounded", isGrounded);
        animator.SetFloat("VerticalVelocity", rigibody.velocity.y);
        // Animator
        isAttacking = animator.GetCurrentAnimatorStateInfo(0).IsTag("Attack");

        // Long idle
        if (animator.GetCurrentAnimatorStateInfo(0).IsTag("Idle"))
        {
            longIdleTimer += Time.deltaTime;

            if (longIdleTimer >= longIdleTime)
            {
                animator.SetTrigger("LongIdle");
            }
        }
        else
        {
            longIdleTimer = 0f;
        }
    }
 
    private void Flip()
    {
        facingRight = !facingRight;
        float localScaleX = transform.localScale.x;
        localScaleX = localScaleX * -1f;
        transform.localScale = new Vector3(localScaleX, transform.localScale.y, transform.localScale.z);
    }

    private void UpdateAttack()
    {
        // Wanna attack?
        if (Input.GetButtonDown("Fire1") && isGrounded && !isAttacking)
        {
            movement = Vector2.zero;
            rigibody.velocity = Vector2.zero;
            animator.SetTrigger("Attack");
        }
    }

    private void UpdateMovement()
    {
        if (isAttacking) return;
        // Movement
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        movement = new Vector2(horizontalInput, 0f);
        // Flip character
        if (horizontalInput < 0f && facingRight)
        {
            Flip();
        }
        else if (horizontalInput > 0f && !facingRight)
        {
            Flip();
        }
        // Is grounded?
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, groundLayer);
        // Is jumpinig?
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            rigibody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
    }
}
