﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserController : MonoBehaviour
{
    private float acc;
    private float hVelocity;
    private float vVelocity;
    private float maxSpeed;
    private SpriteRenderer sprite;
    private Jump jump;
    private Animator animator;
    private void Awake()
    {
        animator = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        jump = GetComponent<Jump>();
    }
    // Start is called before the first frame update
    void Start()
    {
        this.acc = 0.1f;
        this.hVelocity = 0f;
        this.maxSpeed = 1.25f;
        SetHorizontalVelocity(0f);
        SetVerticalVelocity(0f);
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");

        if (horizontal != 0f && !jump.isJumping())
        {
            if (GetHorizontalVelocity() < maxSpeed)
            {
                SetHorizontalVelocity(hVelocity + acc);
            }

            Vector2 direction = new Vector2(horizontal * GetHorizontalVelocity() * Time.deltaTime, 0);
            transform.Translate(direction);
            sprite.flipX = horizontal < 0f;
        }
        else
        {
            SetHorizontalVelocity(0f);
            UpdateJump();
        }
    }

    private void UpdateJump()
    {
        if (jump != null && jump.isJumping())
        {
            animator.SetTrigger("Jump");
            SetVerticalVelocity(jump.GetVercialVelocity());
        }
        else
        {
            SetVerticalVelocity(0f);
        }
        this.animator.SetBool("Idle", GetVerticalVelocity() > -0.1f && GetVerticalVelocity() < 0.1f);
    }

    public float GetHorizontalVelocity()
    {
        return this.hVelocity;
    }

    public float GetVerticalVelocity()
    {
        return this.vVelocity;
    }

    public void SetHorizontalVelocity(float hVelocity)
    {
        this.hVelocity = hVelocity;
        if (animator != null)
        {
            this.animator.SetBool("Idle", hVelocity > -0.1f && hVelocity < 0.1f);
            this.animator.SetFloat("HorizontalVelocity", hVelocity);
        }
    }

    public void SetVerticalVelocity(float vVelocity)
    {
        this.vVelocity = vVelocity;
        if (animator != null)
        {
            this.animator.SetFloat("VerticalVelocity", vVelocity);
        }
    }
}
