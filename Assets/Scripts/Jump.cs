﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    private bool isJump;
    private float vVelocity;
    public float initialVelocity;
    public float gravity;
    private float frame;
    private float yOrigin;
    // Start is called before the first frame update
    void Start()
    {
        gravity = 0.01f;
        initialVelocity = 10f;
        frame = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            if (!isJump)
            {
                isJump = true;
                vVelocity = initialVelocity;
                yOrigin = transform.position.y;
                frame = 0;
            }
        }
        if (transform.position.y >= yOrigin && isJump)
        {
            float y = yOrigin + (vVelocity * frame) - (gravity / 2) * frame * frame;
            Vector2 movement = new Vector2(0, y * Time.deltaTime);
            transform.Translate(movement);

            vVelocity -= gravity;
            frame += 0.01f;
        }
        else
        {
            vVelocity = 0f;
            isJump = false;
        }
    }

    public float GetVercialVelocity()
    {
        return vVelocity;
    }
    public bool isJumping()
    {
        return isJump;
    }
}
