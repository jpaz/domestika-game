﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instanciate : MonoBehaviour
{
    public GameObject prefab;
    public Transform point;
    public float livingTime;
    public void Instantiate()
    {
        GameObject instantiateGameObject = Instantiate(prefab, point.position, Quaternion.identity) as GameObject;
        if (livingTime > 0f)
        {
            Destroy(instantiateGameObject, livingTime);
        }
    }
}
